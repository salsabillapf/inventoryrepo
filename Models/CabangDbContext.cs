﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InventoryApi.Models
{
    public class CabangDbContext : DbContext
    {
        public DbSet<CabangModel> Cabang { get; set; }


        public CabangDbContext(DbContextOptions<CabangDbContext> options) : base(options)
        {
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<CabangModel>().ToTable("tbl_cabang");
        }
    }
}
