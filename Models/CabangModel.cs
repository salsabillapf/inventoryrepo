﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace InventoryApi.Models
{
    public class CabangModel
    {
        [Key]
        public int id_cabang { get; set; }
        public string code_cabang { get; set; }
        public string nama_cabang { get; set; }
        public string pic { get; set; }
    }

    public class CabangModelParam
    {
        [Key]
        public int id_cabang { get; set; }
        public string code_cabang { get; set; }
        public string nama_cabang { get; set; }
        public string pic { get; set; }
    }

    public class Select2
    {
        public int id { get; set; }
        public string text { get; set; }
    }

    public class Id
    {
        public int id { get; set; }
    }

    public class ResponseMessage<T>
    {
        public int statusCode { get; set; }
        public string message { get; set; }
        public List<T> data { get; set; }
    }

    public class ResponseMessageOne<T>
    {
        public int statusCode { get; set; }
        public string message { get; set; }
        public T data { get; set; }
    }
}
