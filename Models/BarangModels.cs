﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace InventoryApi.Models
{
    public class BarangModels
    {
        [Key]
        public int id_barang { get; set; }
        public string code_barang { get; set; }
        public string nama_barang { get; set; }
        public int total{ get; set; }
        public int id_cabang { get; set; }
    }
}
