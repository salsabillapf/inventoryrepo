﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InventoryApi.Models
{
    public class BarangDbContext : DbContext
    {
        public DbSet<BarangModels> Barang { get; set; }


        public BarangDbContext(DbContextOptions<BarangDbContext> options) : base(options)
        {
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<BarangModels>().ToTable("tbl_barang");
        }
    }
}
