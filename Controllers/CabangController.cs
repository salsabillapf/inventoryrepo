﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using InventoryApi.Models;

namespace InventoryApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CabangController : ControllerBase
    {
        private readonly CabangDbContext _context;
        private readonly BarangDbContext _contextBarang;

        public CabangController(CabangDbContext context, BarangDbContext contextBarang)
        {
            _context = context;
            _contextBarang = contextBarang;
        }

        // GET: api/CabangAll
        [HttpGet("GetCabangAll")]
        public async Task<IActionResult> GetCabangAll()
        {
            var responseMsg = new ResponseMessage<CabangModel>() { };
            try
            {
                var dt = _context.Cabang.ToList();

                if ( dt != null && dt.Count > 0)
                {
                    responseMsg = new ResponseMessage<CabangModel>
                    {
                        statusCode = 200,
                        message = "Success",
                        data = dt
                    };
                }
                else
                {
                    responseMsg = new ResponseMessage<CabangModel>
                    {
                        statusCode = 200,
                        message = "No Data",
                        data = null
                    };
                }
            }
            catch (Exception ex)
            {
                responseMsg = new ResponseMessage<CabangModel>
                {
                    statusCode = 500,
                    message = "Internal Server Error " + ex.Message,
                    data = null
                };
            }

            return Ok(responseMsg);
        }

        // GET: api/Cabang
        [HttpGet("GetCabang")]
        public async Task<ActionResult> GetCabang()
        {
            var responseMsg = new ResponseMessage<Select2>() { };
            List<Select2> dt = new List<Select2>() { };
            try
            {
                dt = (from a in _context.Cabang
                      select new Select2
                      {
                          id = a.id_cabang,
                          text = a.code_cabang + " - " + a.nama_cabang
                      }).ToList();

                if (dt != null )
                {
                    responseMsg = new ResponseMessage<Select2>
                    {
                        statusCode = 200,
                        message = "Success",
                        data = dt
                    };
                }
                else
                {
                    responseMsg = new ResponseMessage<Select2>
                    {
                        statusCode = 200,
                        message = "No Data",
                        data = null
                    };
                }
            }
            catch (Exception ex)
            {
                responseMsg = new ResponseMessage<Select2>
                {
                    statusCode = 500,
                    message = "Internal Server Error " + ex.Message,
                    data = null
                };
            }

            return Ok(responseMsg);
        }

        // GET: api/Cabang/5
        [HttpGet("{id}")]
        public async Task<ActionResult> GetCabangModel(int id)
        {
            var responseMsg = new ResponseMessageOne<CabangModel>() { };
            try
            {
                if ( id > 0)
                {
                    var dt = (from a in _context.Cabang
                              where id == a.id_cabang
                              select new CabangModel
                              {
                                  id_cabang = a.id_cabang,
                                  code_cabang = a.code_cabang,
                                  nama_cabang = a.nama_cabang,
                                  pic = a.pic
                              }).FirstOrDefault();

                    if (dt != null)
                    {
                        responseMsg = new ResponseMessageOne<CabangModel>
                        {
                            statusCode = 200,
                            message = "Success",
                            data = dt
                        };
                    }
                    else
                    {
                        responseMsg = new ResponseMessageOne<CabangModel>
                        {
                            statusCode = 200,
                            message = "No Data",
                            data = null
                        };
                    }
                }
                else
                {
                    responseMsg = new ResponseMessageOne<CabangModel>
                    {
                        statusCode = 203,
                        message = "ID can't empty",
                        data = null
                    };
                }
            }
            catch (Exception ex)
            {
                responseMsg = new ResponseMessageOne<CabangModel>
                {
                    statusCode = 500,
                    message = "Internal Server Error " + ex.Message,
                    data = null
                };
            }

            return Ok(responseMsg);
        }

        // PUT: api/Cabang/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutCabangModel(int id, CabangModel cabangModel)
        {
            var responseMsg = new ResponseMessageOne<CabangModel>() { };
            try
            {
                if (id > 0 && cabangModel.id_cabang > 0) 
                {
                    var dt = (from a in _context.Cabang
                              where id == a.id_cabang
                              select new CabangModel
                              {
                                  id_cabang = a.id_cabang,
                                  code_cabang = a.code_cabang,
                                  nama_cabang = a.nama_cabang,
                                  pic = a.pic
                              }).FirstOrDefault();

                    if (dt != null)
                    {
                        _context.Entry(cabangModel).State = EntityState.Modified;
                        await _context.SaveChangesAsync();
                        responseMsg = new ResponseMessageOne<CabangModel>
                        {
                            statusCode = 200,
                            message = "Success",
                            data = dt
                        };
                    }
                    else
                    {
                        responseMsg = new ResponseMessageOne<CabangModel>
                        {
                            statusCode = 200,
                            message = "No Data",
                            data = null
                        };
                    }
                }
                else
                {
                    responseMsg = new ResponseMessageOne<CabangModel>
                    {
                        statusCode = 203,
                        message = "ID can't empty",
                        data = null
                    };
                }
            }
            catch (Exception ex)
            {
                responseMsg = new ResponseMessageOne<CabangModel>
                {
                    statusCode = 500,
                    message = "Internal Server Error " + ex.Message,
                    data = null
                };
            }
            return Ok(responseMsg);
        }

        // POST: api/Cabang
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult> PostCabangModel(CabangModel cabangModel)
        {
            var responseMsg = new ResponseMessageOne<CabangModel>() { };
            try
            {
                if (String.IsNullOrEmpty(cabangModel.code_cabang) || String.IsNullOrEmpty(cabangModel.nama_cabang))
                {
                    responseMsg = new ResponseMessageOne<CabangModel>
                    {
                        statusCode = 203,
                        message = "Code and Name can't empty",
                        data = null
                    };
                }
                else if (cabangModel.id_cabang != 0)
                {
                    responseMsg = new ResponseMessageOne<CabangModel>
                    {
                        statusCode = 203,
                        message = "Set Id with 0 value",
                        data = null
                    };
                }
                else
                {
                    _context.Cabang.Add(cabangModel);
                    await _context.SaveChangesAsync();
                    responseMsg = new ResponseMessageOne<CabangModel>
                    {
                        statusCode = 200,
                        message = "Success",
                        data = null
                    };
                }
            }
            catch (Exception ex)
            {
                responseMsg = new ResponseMessageOne<CabangModel>
                {
                    statusCode = 500,
                    message = "Internal Server Error " + ex.Message,
                    data = null
                };
            }
            return Ok(responseMsg);

            //return CreatedAtAction("GetCabangModel", new { id = cabangModel.id_cabang }, cabangModel);
        }

        // DELETE: api/Cabang/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteCabangModel(int id)
        {
            var responseMsg = new ResponseMessageOne<object>() { };
            try
            {
                if (id > 0)
                {
                    var dt = (from a in _contextBarang.Barang
                              where id == a.id_cabang
                              select new Id
                              {
                                  id = a.id_barang,
                              }).FirstOrDefault();

                    if (dt != null)
                    {
                        responseMsg = new ResponseMessageOne<object>
                        {
                            statusCode = 200,
                            message = "can't be deleted, branches have items",
                            data = null
                        };
                    }
                    else
                    {

                        var cabangModel = await _context.Cabang.FindAsync(id);
                        if (cabangModel == null)
                        {
                            responseMsg = new ResponseMessageOne<object>
                            {
                                statusCode = 200,
                                message = "No Data",
                                data = null
                            };
                        }

                        _context.Cabang.Remove(cabangModel);
                        await _context.SaveChangesAsync();
                        responseMsg = new ResponseMessageOne<object>
                        {
                            statusCode = 200,
                            message = "Success",
                            data = null
                        };
                    }
                }
                else
                {
                    responseMsg = new ResponseMessageOne<object>
                    {
                        statusCode = 203,
                        message = "ID can't empty",
                        data = null
                    };
                }
            }
            catch (Exception ex)
            {
                responseMsg = new ResponseMessageOne<object>
                {
                    statusCode = 500,
                    message = "Internal Server Error " + ex.Message,
                    data = null
                };
            }
            return Ok(responseMsg);
        }

        private bool CabangModelExists(int id)
        {
            return _context.Cabang.Any(e => e.id_cabang == id);
        }
    }
}
